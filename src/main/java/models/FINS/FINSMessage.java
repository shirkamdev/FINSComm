package models.FINS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import models.Variable;

/**
 * Stores the message so you can send it via socket to PLC.
 */
public class FINSMessage {
	
	/**
	 * The header code and type of message.
	 */
	private FINSCode code;
	
	/**
	 * To which variable we reference.
	 */
	private List<Variable> variables;
	
	/**
	 * The message that we will send.
	 */
	private int[] message = {};
	
	private FINSMessage() {
		this.variables = new ArrayList<>();
	}
	
	/**
	 * Instantiate a FINSMessage without variable (now can only be done when connecting).
	 * @param code The code of the operation to perform
	 */
	public FINSMessage(FINSCode code) {
		this(code, (Variable) null);
	}
	
	/**
	 * Instantiate a message with a given variable.
	 * @param code The code of the operation to perform.
	 * @param var The variable we are gonna use.
	 * @see FINSCode FINSCode For details about operation codes.
	 */
	public FINSMessage(FINSCode code, Variable var) {
		this();
		
		if(!code.equals(FINSCode.CONNECT) && var == null) {
			throw new IllegalArgumentException("You MUST specify a variable.");
		}
		
		this.code = code;
		this.variables.add(var);
		
		setupMessage();
	}
	
	/**
	 * Instantiate a message to use multiple variables. This can only be possible to use with the 
	 * MULTIPLE_* operation codes.
	 * @param code The code of the operation to perform, it must allow to use more than one 
	 * variable.
	 * @param vars The variables we are gonna use.
	 * @see FINSCode FINSCode For details about operation codes.
	 */
	public FINSMessage(FINSCode code, Collection<Variable> vars) {
		this();
		
		switch (code) {
		case MULTIPLE_MEMORY_AREA_READ:
			// https://www.youtube.com/watch?v=7R1nRxcICeE
			break;

		default:
			throw new IllegalArgumentException("You must provide a FINS CODE that uses multiple "
					+ "variables.");
		}
		
		if(vars.isEmpty())
			throw new IllegalArgumentException("You MUST specify at least one variable.");
		
		this.code = code;
		
		for(Variable var: vars)
			this.variables.add(var);
		
		setupMessage();
	}
	
	/**
	 * Instantiate a message to use multiple variables. This can only be possible to use with the 
	 * MULTIPLE_* operation codes.
	 * @param code The code of the operation to perform, it must allow to use more than one 
	 * variable.
	 * @param vars The variables we are gonna use.
	 * @see FINSCode FINSCode For details about operation codes.
	 */
	public FINSMessage(FINSCode code, Variable... vars) {
		this();
		
		switch (code) {
		case MULTIPLE_MEMORY_AREA_READ:
			break;

		default:
			throw new IllegalArgumentException("You must provide a FINS CODE that uses multiple "
					+ "variables.");
		}
		
		if(vars.length == 0)
			throw new IllegalArgumentException("You MUST specify at least one variable.");
		
		this.code = code;
		
		for(Variable var: vars)
			this.variables.add(var);
		
		setupMessage();
	}
	
	/**
	 * Converts the message to an array of bytes, in order to send it.
	 * @return
	 */
	public byte[] getMessageBytes() {
        byte res[] = new byte[message.length];
        for (int i = 0; i < message.length; i++) {
            res[i] = (byte) message[i];
        }
        
        return res;
    }
	
	/**
	 * Setups the message taking in consider the code of it.
	 */
	private void setupMessage() {
		message = new int[] {};
		append(FINSConstants.FINS_HEADER);
		append(FINSConstants.FINS_LENGTH_PLACEHOLDER);
		
		
		
		
		switch (this.code) {
		case CONNECT:
			append(FINSConstants.FINS_COMMAND_CONNECT);
			append(FINSConstants.FINS_ERROR_CODE);
			append(FINSConstants.FINS_CLIENT_NODE);
			//append(FINSConstants.FINS_COMMAND_CONNECT);
			//append(FINSConstants.FINS_CONNECT);
			
			setupConnectLength();
			break;
		
		// TODO add more codes
		case MEMORY_AREA_READ:
			append(FINSConstants.FINS_COMMAND_GENERIC);
			append(FINSConstants.FINS_ERROR_CODE);
			setupReadMessage();
			updateMessageLength();
			break;
			
		case MULTIPLE_MEMORY_AREA_READ:
			append(FINSConstants.FINS_COMMAND_GENERIC);
			append(FINSConstants.FINS_ERROR_CODE);
			setupMultipleMemAreaRead();
			updateMessageLength();
			break;
		}
		
		
	}
	
	/**
	 * Setups a {@link FINSCode#MEMORY_AREA_READ } message. 
	 */
	private void setupReadMessage() {
		// Setup generic message
		setupFINSHeader(FINSConstants.DEFAULT_GCT, FINSConstants.DEFAULT_DNA, 
				/*FINSConstants.DEFAULT_DA1*/ 0x4B, FINSConstants.DEFAULT_DA2, FINSConstants.DEFAULT_SNA,
				/*FINSConstants.DEFAULT_SA1*/ 0xEF, FINSConstants.DEFAULT_SA2, FINSConstants.DEFAULT_SID);
		
		//Configure reading for our referenced variable
        append(variables.get(0).getMemoryArea());
        append(toWord(variables.get(0).getMemoryAddress()));
        append(0); // To finish mem address
        append(0); // Whole variable
        append(1); // 1 Address
	}
	
	/**
	 * Setups a {@link FINSCode#MULTIPLE_MEMORY_AREA_READ } message. 
	 */
	private void setupMultipleMemAreaRead() {
		// TODO change DA1 and SA1
		// Setup generic message
		setupFINSHeader(FINSConstants.DEFAULT_GCT, FINSConstants.DEFAULT_DNA, 
				/*FINSConstants.DEFAULT_DA1*/ 0x4B, FINSConstants.DEFAULT_DA2, FINSConstants.DEFAULT_SNA,
				/*FINSConstants.DEFAULT_SA1*/ 0xEF, FINSConstants.DEFAULT_SA2, FINSConstants.DEFAULT_SID);
		
		//Add which variables we want to read
		for(Variable var: variables) {
			append(var.getMemoryArea());
        	append(toWord(var.getMemoryAddress()));
        	append(0); // To finish mem address
		}
	}
	
	/**
	 * Setups the FINS header of the message
	 * @param permissibleNumberOfGateways The permissible number of gateways for the message. Most 
	 * of the time, you can leave this as the default value at {@link FINSConstants}.
	 * @param destinationNetworkAddress  The address where the message is headed. Most of the time, 
	 * you can leave this as the default value at {@link FINSConstants}.
	 * @param destinationNodeAddress The node where the message is headed. This node is given in the
	 * connect message if you don't know it.
	 * @param destinationUnitAddress The unit where the message is headed. Most of the time, you can
	 * leave this as the default value at {@link FINSConstants}.
	 * @param sourceNetworkAddress The source network of the message. Most of the time, you can 
	 * leave this as the default value at {@link FINSConstants}.
	 * @param sourceNodeAddress The source node of the message. This node is given in the connect 
	 * message if you don't know it.
	 * @param sourceUnitAddress The source unit of the message. Most of the time, you can leave this
	 * as the default value at {@link FINSConstants}.
	 * @param sourceId The ID of the communication. Most of the time, you can leave this as the 
	 * default value at {@link FINSConstants}.
	 */
	private void setupFINSHeader(int permissibleNumberOfGateways, int destinationNetworkAddress, 
			int destinationNodeAddress, int destinationUnitAddress, int sourceNetworkAddress, 
			int sourceNodeAddress, int sourceUnitAddress, int sourceId) {
        append(FINSConstants.ICF_COMMAND);
        append(FINSConstants.RSV);
        append(permissibleNumberOfGateways);
        append(destinationNetworkAddress);
        append(destinationNodeAddress);
        append(destinationUnitAddress);
        append(sourceNetworkAddress);
        append(sourceNodeAddress);
        append(sourceUnitAddress);
        append(sourceId);
        append(code.codes);
	}
	
	/**
     * Appends bytes to the end of the message.
     * 
     * @param bytes An integer array with the bytes to append
     */
    private void append(int[] bytes) {
        int[] res = new int[message.length + bytes.length];
        
        System.arraycopy(message, 0, res, 0, message.length);
        System.arraycopy(bytes, 0, res, message.length, bytes.length);
        
        message = res;
    }
    
    /**
     * Appends a single byte to the end of the message.
     * 
     * @param singleByte The byte to append
     */
    private void append(int singleByte) {
        int[] toAppend = {singleByte};
        append(toAppend);
    }
    
    /**
     * Calculates the length of the messages and writes it to the bytes which store this information.<br>
     * (These bytes are defined by {@link #FINS_LENGTH_BYTES})
     */
    private void updateMessageLength() {
        int length = message.length - 8;
        
        int[] lengthBytes = {
                (byte) (length >>> 24),
                (byte) (length >>> 16),
                (byte) (length >>> 8),
                (byte) length
        };
        
        for (int i : FINSConstants.FINS_LENGTH_BYTES) {
            message[i] = lengthBytes[i-4];
        }
    }
    
    /**
     * The connect message length is always the same
     */
    private void setupConnectLength() {
    	int[] lengthBytes = {
    			0x00,
    			0x00,
    			0x00,
    			0x0C
    	};
    	
    	for (int i : FINSConstants.FINS_LENGTH_BYTES) {
            message[i] = lengthBytes[i-4];
        }
    }
    
    /**
     * Converts a number to a word.
     * 
     * @param hexNumber The number to convert
     * @return A two element integer array where the first element represents the upper byte of the word and the second element represents the lower byte of the word
     */
    private int[] toWord(int hexNumber) {
        return new int[] {
                (byte) (hexNumber >>> 8),
                (byte) hexNumber
        };
    }
    
    /**
     * Converts an array of numbers to an array of words.
     * 
     * @param hexNumberArray The array of numbers
     * @return An array of integers which contains pairs of integers. For each pair the first element represents the upper byte of the word and the second element represents the lower byte of the word
     */
    private int[] toWord(int[] hexNumberArray) {
        int[] res = new int[hexNumberArray.length * 2];
        
        for (int i = 0, j = 0; i < res.length; i += 2, j++) {
            int[] word = toWord(hexNumberArray[j]);
            res[i] = word[0];
            res[i+1] = word[1];
        }
        
        return res;
    }
	
}
