package models.FINS;

/**
 * Stores FINS constants.
 */
public class FINSConstants {
	
	/** 
	 * FINS header bytes. It says FINS in hex.
	 */
    public static final int[] FINS_HEADER = {0x46, 0x49, 0x4e, 0x53};
    
    /** 
     * Placeholder for the bytes describing the length of the message.
     * Must be copied and edited.
     */
    public static final int[] FINS_LENGTH_PLACEHOLDER = 
    	{0x00, 0x00, 0x00, 0x00};
    
    /** 
     * Placeholder for the error code.
     */
    public static final int[] FINS_ERROR_CODE = 
    	{0x00, 0x00, 0x00, 0x00};
    
    /** 
     * FINS Client (us) node address. As we are using TCP, if we set it to 0, the PLC asigns
     * one to us.
     */
    public static final int[] FINS_CLIENT_NODE = 
    	{0x00, 0x00, 0x00, 0x00};
    
    /** 
     * FINS connection command. It is an empty command, because FINS needs
     * an empty message to start comms.
     */
    public static final int[] FINS_COMMAND_CONNECT = 
    	{0x00, 0x00, 0x00, 0x00};
    
    /** 
     * FINS generic command.
     */
    public static final int[] FINS_COMMAND_GENERIC = 
    	{0x00, 0x00, 0x00, 0x02};
    
    /** 
     * FINS connect message payload. It's empty.
     */
    public static final int[] FINS_CONNECT = {0x00, 0x00, 0x00, 0x00};
    
    /** 
     * The bytes in a FINS message which describe the length of the message. 
     */
    public static final int[] FINS_LENGTH_BYTES = {4, 5, 6, 7};
    
    /** 
     * FINS message command identifier 
     */
    public static final int ICF_COMMAND = 0x80;
    
    /** 
     * FINS message response identifier 
     */
    public static final int ICF_RESPONSE = 0xC0;
    
    /** 
     * FINS reserved byte 
     */
    public static final int RSV = 0x00;
    
    /**
     * Time to wait a response from PLC, in ms
     */
    public static final int SLEEP_TIME_RESPONSE = 100;
    
    /**
     * Max 0x07
     */
    public static final int DEFAULT_GCT = 0x03;
    /**
     * Destination network address, 0x00 = local
     */
    public static final int DEFAULT_DNA = 0x00;
    /**
     * Destination node address, host-link
     */
    public static final int DEFAULT_DA1 = 0x01;
    /**
     * Destination unit address, 0x00 = CPU
     */
    public static final int DEFAULT_DA2 = 0x00;
    /**
     * Source network address, 0x00 = local
     */
    public static final int DEFAULT_SNA = 0x00;
    /**
     * Source node address, 0x00 = CPU
     */
    public static final int DEFAULT_SA1 = 0x00;
    /**
     * source unit address, 0x00 = local
     */
    public static final int DEFAULT_SA2 = 0x00;
    /**
     * Source ID
     */
    public static final int DEFAULT_SID = 0x01; 
    
	// Not instantiable
    private FINSConstants() {}
}
