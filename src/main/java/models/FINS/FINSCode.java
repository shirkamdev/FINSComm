package models.FINS;

/**
 * Stores all available FINS Operations with its FINS codes. 
 */
public enum FINSCode {
	/**
	 * Connect operation. This operation MUST be done always before trying to do any other operation
	 * with the PLC.
	 */
	CONNECT(0x00, 0x00),
	/**
	 * This operation allows to read one or more CONSECUTIVE variables from a memory address. You 
	 * must specify the MEMORY AREA CODE, the starting MEMORY ADDRESS, the bytes to read (0 if whole
	 * variable) and the number of variables to read. <br/><br/>
	 * The look of the command frame is like:<br/>
	 * <table border="1" style="border: solid 1px">
	 * 	<tr>
	 * 		<th colspan="2" >OP Code</th>
	 * 		<th>MEM AREA CODE</th>
	 * 		<th colspan="3" style="">MEM ADDRESS</th>
	 * 		<th>Bits to read</th>
	 * 		<th>N vars to read</th>
	 * 	</tr>
	 * 	<tr style="text-align: center">
	 * 		<td>0x01</td>
	 * 		<td>0x01</td>
	 * 		<td>0x82</td>
	 * 		<td>0x02</td>
	 * 		<td>0x25</td>
	 * 		<td>0x00</td>
	 * 		<td>0x00</td>
	 * 		<td>0x01</td>
	 * 	</tr>
	 * </table>
	 */
	MEMORY_AREA_READ(0x01, 0x01),
	// MEMORY_AREA_WRITE(0x01, 0x02),
	/**
	 * This operation allows to read multiple variables, consecutives or not. You must specify the
	 * MEMORY AREA CODE and the MEMORY ADDRESS for each of them. <br/><br/>
	 * The look of the command frame is like:<br/>
	 * <table border="1" style="border: solid 1px">
	 * 	<tr>
	 * 		<th colspan="2" >OP Code</th>
	 * 		<th>MEM AREA CODE</th>
	 * 		<th colspan="3" style="">MEM ADDRESS</th>
	 * 		<th></th>
	 * 		<th>MEM AREA CODE</th>
	 * 		<th colspan="3" style="">MEM ADDRESS</th>
	 * 	</tr>
	 * 	<tr style="text-align: center">
	 * 		<td>0x01</td>
	 * 		<td>0x04</td>
	 * 		<td>0x82</td>
	 * 		<td>0x02</td>
	 * 		<td>0x25</td>
	 * 		<td>0x00</td>
	 * 		<td>. . .</td>
	 * 		<td>0x82</td>
	 * 		<td>0x02</td>
	 * 		<td>0x2C</td>
	 * 		<td>0x00</td
	 * 	</tr>
	 * </table>
	 */
	MULTIPLE_MEMORY_AREA_READ(0x01, 0x04);
	
	/**
	 * Stores the two FINS codes used in comunication.
	 */
	public final int[] codes;
	
	private FINSCode(int firstCode, int secondCode) {
		codes = new int[]{firstCode, secondCode};
	}
	
	public int getTypeCode() {
		return codes[0];
	}
	
	public int getCommandCode() {
		return codes[1];
	}
}
