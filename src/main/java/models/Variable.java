package models;

import java.time.Duration;
import java.time.LocalDateTime;

import configuration.Configuration;

/**
 * Represents a PLC variable. As every variable, it has a memory direction,
 * an IEC type, which has a size, and a value.
 * As this object is mirror from a PLC variable, it stores the last time
 * the variable was updated. If this time overcomes a threshold, it tries to
 * communicate with the device to obtain a newer value.
 * User discretion is advised, as it could overload the network if a small
 * enough threshold is set.
 * @author shirkam
 * @version 1.0
 */
public class Variable {
	
	private IECTypes type;
	private int memoryAddress;
	private int memoryArea;
	private String name;
	private String value;
	/**
	 * To which PLC belongs
	 */
	private PLC plc;
	
	private LocalDateTime lastUpdate;
	
	public Variable(int memoryArea, int memAddress, IECTypes type, PLC plc) {
		this(memoryArea, memAddress, type, "", plc);
	}
	
	public Variable(int memoryArea, int memAddress, IECTypes type, String name, PLC plc) {
		this(memoryArea, memAddress, type, name, "0", plc);
	}

	private Variable(int memoryArea, int memAddress, IECTypes type, String name, String val, 
			PLC plc) {
		this.memoryArea = memoryArea;
		this.type = type;
		this.memoryAddress = memAddress;
		this.name = name;
		this.value = val;
		this.plc = plc;
	}
	
	// We use memory address as our primary hash code.
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + memoryAddress;
		result = prime * result + plc.hashCode();
		//result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variable other = (Variable) obj;
		if (memoryAddress != other.memoryAddress)
			return false;
		if (!this.plc.equals(other.plc))
			return false;
		// TODO Think if this should throw an exception if two variables
		// in the same address don't have the same type.
		//if (type != other.type)
		//	return false;
		return true;
	}

	@Override
	public String toString() {
		return this.name + " (Addr: " + this.memoryAddress + " Type: " 
				+ this.type + "): " + getValueInternal();
	}

	public IECTypes getType() {
		return type;
	}

	public void setType(IECTypes type) {
		this.type = type;
	}

	public int getMemoryAddress() {
		return memoryAddress;
	}

	public void setMemoryAddress(int memoryAddress) {
		this.memoryAddress = memoryAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getValue() {
		///TODO Check last time the var value was updated
		if(Configuration.isVarAutoUpdateActivated() && isLastUpdatedLongAgo())
			this.plc.readVariableValueFromPLC(this);
		
		return getValueInternal();
	}
	
	private String getValueInternal() {
		return value;
	}

	public void setValue(String value) {
		// TODO this has to communicate with the PLC
		this.setValueInternal(value);
	}
	
	// Only access on package
	void setValueInternal(String value) {
		this.value = value;
		this.lastUpdate = LocalDateTime.now();
	}
	
	public int getMemoryArea() {
		return memoryArea;
	}

	public void setMemoryArea(int memoryArea) {
		this.memoryArea = memoryArea;
	}
	
	private boolean isLastUpdatedLongAgo() {
		return lastUpdate == null ||
				Duration.between(lastUpdate, LocalDateTime.now()).toMillis() 
					>= Configuration.getVarAutoUpdateTime();

	}
}
