package configuration;

import java.time.Instant;

/**
 * Allows to perform various operations to show data, either from console, or to a file.
 */
public class Logger {
	
	/**
	 * All HEXADECIMAL digits
	 */
	private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
	
	/**
	 * Shows on stderr the message with a timestamp
	 * @param message The message to show
	 */
	public static void Debug(String message) {
		if(Configuration.isDebug()) {
			System.err.println("["+Instant.now().toString()+"] "+message);
		}
	}
	
	/**
	 * Shows on stderr an array of bytes, as its HEX values, using the 
	 * {@link Logger#HEX_ARRAY Hex char array}.
	 * @param array The array to show
	 */
	public static void Debug(byte[] array) {
		Logger.Debug(Logger.bytesToHex(array));
	}
	
	/**
	 * Shows on stderr an array of ints, as its HEX values.
	 * @see Logger#Debug(byte[])
	 * @param array The array to show
	 */
	public static void Debug(int[] array) {
		Logger.Debug(Logger.intsToHex(array));
	}
	
	/**
	 * Converts an array of bytes to its hexadecimal representation on a String. Uses the 
	 * {@link Logger#HEX_ARRAY Hex char array}.
	 * @param bytes The array to convert
	 * @return The hex conversion from the array
	 */
	private static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    // Convert byte to its HEX representation
	    for (int j = 0; j < bytes.length; j++) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = HEX_ARRAY[v >>> 4];
	        hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
	    }
	    
	    StringBuilder sb = new StringBuilder();
	    for(int i=0; i<hexChars.length; i++) {
	    	sb.append(hexChars[i]);
	    	// Append a space every two characters 
	    	if(i%2 != 0)
	    		sb.append(" ");
	    }
	    
	    return sb.toString();
	}
	
	/**
	 * Converts an array of ints to bytes and then to their HEX representation on a String.
	 * @param bytes The array to convert
	 * @return The hex conversion from the array
	 */
	private static String intsToHex(int[] bytes) {
	    byte[] array = new byte[bytes.length];
	    for(int i=0; i<bytes.length; ++i)
	    	array[i] = (byte) bytes[i];
	    
	    return bytesToHex(array);
	}
	
	// Cannot be instantiated
	private Logger() {}
}
