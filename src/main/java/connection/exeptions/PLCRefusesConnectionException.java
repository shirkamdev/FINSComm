package connection.exeptions;

public class PLCRefusesConnectionException extends Exception {
	private static final long serialVersionUID = 6522011368521942648L;
	private String ip;
	private int port;
	
	public PLCRefusesConnectionException(String ip, int port) {
		super("The PLC located at "+ip+":"+port+" is not responding "
				+ "or refuses the FINS connect message.");
		this.ip = ip;
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}	
