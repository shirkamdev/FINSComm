package connection.exeptions;

public class PLCConnectErrorException extends Exception {
	private static final long serialVersionUID = -6905075654969734693L;
	private String ip;
	private int port;
	
	public PLCConnectErrorException(String ip, int port) {
		super("Error while trying to connect to PLC in " + ip + ":" + port
				+". Check that the PLC is available and reachable from the"
				+ "device and try again.");
		this.ip = ip;
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	
}
