package connection.exeptions;

public class NoPLCResponseException extends Exception {

	public NoPLCResponseException() {
		super("There was no response from PLC.");
	}
	
}
