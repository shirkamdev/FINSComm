package connection.exeptions;

public class CouldNotAwaitForResponseException extends Exception {
	private static final long serialVersionUID = -4592731184465958459L;

	public CouldNotAwaitForResponseException() {
		super("Error: Could not wait for response while comunicating with PLC.");
	}
}
